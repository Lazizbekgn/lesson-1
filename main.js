let word = 'dsadsaHJ jksjdaJKJS JKs ddsadkasl'

function upperCase(str) {
    let arr = str.toLocaleLowerCase().split(" ")
    let result = arr.map(function(val) {
        return val.replace(val.charAt(0), val.charAt(0).toUpperCase())
    })
    return result.join(" ")
}
console.log(upperCase(word));
// ------------------------------

// I tried to do this task without function but I did get the expexted result in the end
// for(let i = 0; i < arr.length; i++) {
//     let val = arr[i]
//     val.replace(val.charAt(0), val.charAt(0).toUpperCase())
// }
// let result = arr.join(" ")
// console.log(result);